{
    "DeliveryStreamName": "${delivery_stream_name}",
    "DeliveryStreamType": "KinesisStreamAsSource",
    "KinesisStreamSourceConfiguration": {
        "KinesisStreamARN": "${source_stream_arn}",
        "RoleARN": "${role_arn}"
    },
    "RedshiftDestinationConfiguration": {
        "RoleARN": "${role_arn}",
        "ClusterJDBCURL": "${redshift_cluster_jdbc_url}",
        "CopyCommand": {
            "CopyOptions": "gzip json '\''${mapping_url}'\''",
            "DataTableName": "${table_name}"
        },
        "Username": "${username}",
        "Password": "${password}",
        "S3Configuration": {
            "BufferingHints": {
                "IntervalInSeconds": 60
            },
            "RoleARN": "${role_arn}",
            "BucketARN": "${s3_bucket_arn}",
            "CompressionFormat": "GZIP"
        },
        "CloudWatchLoggingOptions": {
            "Enabled": true,
            "LogGroupName": "${cloudwatch_log_group}",
            "LogStreamName": "${cloudwatch_log_stream}"
        }
    }
}