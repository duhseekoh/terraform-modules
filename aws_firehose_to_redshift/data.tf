locals {
  qualified_name = "${var.env}-${var.name}"
}

data "template_file" "create_delivery_stream_json" {
  template = "${file("${path.module}/create-delivery-stream.json.tpl")}"

  vars {
    delivery_stream_name = "${local.qualified_name}"
    source_stream_arn    = "${var.source_stream_arn}"
    role_arn             = "${aws_iam_role.role.arn}"

    s3_bucket_arn = "${var.s3_bucket_arn}"
    s3_prefix     = "${var.s3_prefix}"

    redshift_cluster_jdbc_url = "jdbc:redshift://${var.redshift_cluster_endpoint}/${var.redshift_database_name}"
    username                  = "${var.redshift_username}"
    password                  = "${var.redshift_password}"
    table_name                = "\\\"${var.redshift_table_name}\\\""

    cloudwatch_log_group  = "${aws_cloudwatch_log_group.log_group.name}"
    cloudwatch_log_stream = "${aws_cloudwatch_log_stream.log_stream.name}"

    mapping_url = "${var.mapping_url}"
  }
}
