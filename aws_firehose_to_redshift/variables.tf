variable "env" {}
variable "region" {}
variable "name" {}

variable "redshift_security_group_id" {}
variable "redshift_cluster_endpoint" {}
variable "redshift_database_name" {}
variable "redshift_username" {}
variable "redshift_password" {}
variable "redshift_table_name" {}
variable "mapping_url" {}

variable "s3_bucket_arn" {}

variable "s3_prefix" {
  default = ""
}

variable "source_stream_arn" {}

variable "firehose_cidr_blocks" {
  default = {
    "us-east-1"      = ["52.70.63.192/27"]
    "us-east-2"      = ["13.58.135.96/27"]
    "us-west-2"      = ["52.89.255.224/27"]
    "ap-northeast-1" = ["13.113.196.224/27"]
    "eu-central-1"   = ["35.158.127.160/27"]
    "eu-west-1"      = ["52.19.239.192/27"]
  }
}
