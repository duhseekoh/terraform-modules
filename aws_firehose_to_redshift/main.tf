// cloudwatch logging

resource "aws_cloudwatch_log_group" "log_group" {
  name = "${local.qualified_name}"
}

resource "aws_cloudwatch_log_stream" "log_stream" {
  name           = "${local.qualified_name}/firehose"
  log_group_name = "${aws_cloudwatch_log_group.log_group.name}"
}

// security

resource "aws_security_group_rule" "firehose_redshift_ingress" {
  type              = "ingress"
  from_port         = "5439"
  to_port           = "5439"
  protocol          = "tcp"
  cidr_blocks       = ["${var.firehose_cidr_blocks[var.region]}"]
  security_group_id = "${var.redshift_security_group_id}"
}

resource "aws_iam_role" "role" {
  name = "${local.qualified_name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "firehose.amazonaws.com"
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "policy" {
  name = "${local.qualified_name}"
  role = "${aws_iam_role.role.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "kinesis:DescribeStream",
        "kinesis:GetShardIterator",
        "kinesis:GetRecords",
        "kinesis:ListStreams"
      ],
      "Resource": "${var.source_stream_arn}"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:AbortMultipartUpload",        
        "s3:GetBucketLocation",        
        "s3:GetObject",        
        "s3:ListBucket",        
        "s3:ListBucketMultipartUploads",        
        "s3:PutObject"
      ],
      "Resource": [
        "${var.s3_bucket_arn}",
        "${var.s3_bucket_arn}/${var.s3_prefix}*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "logs:PutLogEvents"
      ],
      "Resource": "${aws_cloudwatch_log_stream.log_stream.arn}"
    }
  ]
}
EOF
}

// delivery stream

/*
Terraform does not yet support KinesisStreamAsSource (see 
https://github.com/terraform-providers/terraform-provider-aws/issues/1601).
Since it's an attribute that's specified on creation, we can't use Terraform's
AWS provider to manage creation of the Firehose at all.

The workaround is to use the AWS CLI, templating, and local-exec to manually
create the delivery stream with the necessary configuration.
*/

resource "null_resource" "firehose_delivery_stream" {
  depends_on = ["aws_iam_role_policy.policy", "aws_iam_role.role"]

  triggers {
    template = "${data.template_file.create_delivery_stream_json.rendered}"
  }

  provisioner "local-exec" {
    command = <<END
aws firehose create-delivery-stream \
  --region '${var.region}' \
  --cli-input-json '${replace("${data.template_file.create_delivery_stream_json.rendered}", "\n", "")}'
END
  }

  provisioner "local-exec" {
    when = "destroy"

    # WARNING: This will _always_ succeed. This is necessary because we want it
    # to succeed even if the stream doesn't exist. Returning `true` was easier
    # than trying to conditionally parse the output on failure to detect exactly
    # which failure it was.
    command = <<END
aws firehose delete-delivery-stream \
  --delivery-stream-name ${local.qualified_name} \
  || true
END
  }
}
