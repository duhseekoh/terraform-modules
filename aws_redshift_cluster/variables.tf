variable "env" {}

variable "name" {}

variable "vpc_id" {}

variable "vpc_subnet_ids" {
  type = "map"
}

variable "master_username" {}

variable "master_password" {}

variable "node_type" {
  default = "dc1.large"
}

variable "security_group_ids" {
  type = "list"
}
