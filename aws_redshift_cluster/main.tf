locals {
  qualified_name = "${var.env}-${var.name}"
}

resource "aws_security_group" "security_group" {
  name   = "${local.qualified_name}-redshift-cluster"
  vpc_id = "${var.vpc_id}"

  tags = {
    Name      = "${local.qualified_name}-redshift-cluster"
    terraform = "true"
  }
}

resource "aws_redshift_subnet_group" "subnet_group" {
  name       = "${local.qualified_name}"
  subnet_ids = ["${var.vpc_subnet_ids["public"]}"]
}

resource "aws_redshift_cluster" "redshift_cluster" {
  # Cluster identifier must match /[a-z0-9\-]+/
  cluster_identifier = "${lower(local.qualified_name)}"

  # Database name must match /[a-z0-9_$]+/
  database_name = "${replace(lower(var.name), "-", "_")}"

  master_username     = "${var.master_username}"
  master_password     = "${var.master_password}"
  node_type           = "${var.node_type}"
  cluster_type        = "single-node"
  publicly_accessible = true
  skip_final_snapshot = true

  vpc_security_group_ids = [
    "${concat(var.security_group_ids, list(aws_security_group.security_group.id))}",
  ]

  cluster_subnet_group_name = "${aws_redshift_subnet_group.subnet_group.name}"
}
