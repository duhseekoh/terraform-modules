output "endpoint" {
  value = "${aws_redshift_cluster.redshift_cluster.endpoint}"
}

output "security_group_id" {
  value = "${aws_security_group.security_group.id}"
}

output "database_name" {
  value = "${aws_redshift_cluster.redshift_cluster.database_name}"
}
