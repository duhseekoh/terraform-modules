locals {
  qualified_name = "${var.env}-${var.name}"
}

resource "aws_api_gateway_rest_api" "api" {
  name = "${local.qualified_name}"
}

resource "aws_api_gateway_domain_name" "api" {
  domain_name     = "api.${var.domain}"
  certificate_arn = "${data.aws_acm_certificate.domain.arn}"
}

resource "aws_route53_record" "api" {
  zone_id = "${data.aws_route53_zone.root.zone_id}"
  name    = "${aws_api_gateway_domain_name.api.domain_name}"
  type    = "A"

  alias {
    name                   = "${aws_api_gateway_domain_name.api.cloudfront_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.api.cloudfront_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_iam_role" "integration_role" {
  name = "${local.qualified_name}-api-integration-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
} 
EOF
}
