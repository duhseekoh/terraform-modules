data "aws_acm_certificate" "domain" {
  domain   = "${var.domain}"
  statuses = ["ISSUED"]
}

data "aws_route53_zone" "root" {
  name = "${var.domain}."
}
