output "api_gateway_id" {
  value = "${aws_api_gateway_rest_api.api.id}"
}

output "api_domain_name" {
  value = "${aws_api_gateway_domain_name.api.domain_name}"
}

output "integration_role_arn" {
  value = "${aws_iam_role.integration_role.arn}"
}

output "integration_role_name" {
  value = "${aws_iam_role.integration_role.name}"
}

output "root_resource_id" {
  value = "${aws_api_gateway_rest_api.api.root_resource_id}"
}
