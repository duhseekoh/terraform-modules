variable "service_name" {
  type = "string"
}

variable "vpc_id" {
  type = "string"
}

variable "default_security_group_id" {
  type = "string"
}

variable "default_security_group_ids" {
  type = "map"
}

variable "subnet_type" {
  type        = "string"
  description = "'private' or 'public' subnet"
}

variable "asg_min_size" {
  type    = "string"
  default = "1"
}

variable "asg_max_size" {
  type    = "string"
  default = "1"
}

variable "asg_desired_capacity" {
  type    = "string"
  default = "1"
}

variable "ami_id" {
  type    = "string"
  default = "ami-218eed59"
}

variable "instance_type" {
  type    = "string"
  default = "t2.micro"
}

variable "instance_key_name" {
  type        = "string"
  description = "SSH key for instance"
}

variable "subnet_ids" {
  type = "map"
}
