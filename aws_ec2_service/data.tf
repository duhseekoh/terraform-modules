data "template_file" "cloud_init" {
  template = "${file("${path.module}/cloud_init.tpl")}"
}
