resource "aws_security_group" "security_group" {
  name   = "${var.service_name}"
  vpc_id = "${var.vpc_id}"

  tags = {
    Name      = "${var.service_name}"
    terraform = "true"
  }
}

resource "aws_iam_role" "role" {
  name = "service-${var.service_name}"
  path = "/service/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "profile" {
  name = "service-${var.service_name}"
  path = "/service/"
  role = "${aws_iam_role.role.name}"
}

resource "aws_launch_configuration" "launch_config" {
  name_prefix          = "${var.service_name}-"
  image_id             = "${var.ami_id}"
  instance_type        = "${var.instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.profile.name}"
  key_name             = "${var.instance_key_name}"

  user_data = "${data.template_file.cloud_init.rendered}"

  lifecycle {
    create_before_destroy = true
  }

  security_groups = [
    "${var.default_security_group_id}",
    "${var.default_security_group_ids[var.subnet_type]}",
    "${aws_security_group.security_group.id}",
  ]
}

resource "aws_autoscaling_group" "asg" {
  vpc_zone_identifier  = ["${var.subnet_ids[var.subnet_type]}"]
  name                 = "${var.service_name}"
  min_size             = "${var.asg_min_size}"
  max_size             = "${var.asg_max_size}"
  desired_capacity     = "${var.asg_desired_capacity}"
  force_delete         = true
  launch_configuration = "${aws_launch_configuration.launch_config.name}"

  tag {
    key                 = "terraform"
    value               = "true"
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "${var.service_name}"
    propagate_at_launch = true
  }
}
