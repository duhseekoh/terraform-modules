locals {
  qualified_name = "${var.env}-${var.name}"
}

resource "aws_ecs_cluster" "cluster" {
  name = "${local.qualified_name}"
}

resource "aws_security_group" "cluster" {
  name   = "${local.qualified_name}"
  vpc_id = "${var.vpc_id}"

  tags {
    Name      = "${local.qualified_name}"
    terraform = "true"
  }
}

resource "aws_security_group_rule" "allow_outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.cluster.id}"
}

resource "aws_autoscaling_group" "cluster" {
  name                 = "${local.qualified_name}-autoscaling-group"
  max_size             = "${var.cluster_max_size}"
  min_size             = "${var.cluster_min_size}"
  desired_capacity     = "${var.cluster_desired_size}"
  launch_configuration = "${aws_launch_configuration.cluster.name}"
  vpc_zone_identifier  = ["${var.cluster_subnet_ids}"]

  tag {
    key                 = "Name"
    value               = "${local.qualified_name}-instance"
    propagate_at_launch = true
  }

  tag {
    key                 = "terraform"
    value               = "true"
    propagate_at_launch = true
  }
}

resource "aws_s3_bucket_object" "ecs_config" {
  bucket = "${var.config_bucket}"
  key    = "${var.config_key_prefix}ecs.config"

  content = <<EOF
ECS_CLUSTER=${aws_ecs_cluster.cluster.name}
ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION="10m"
EOF
}

resource "aws_s3_bucket_object" "ecs_userdata" {
  bucket  = "${var.config_bucket}"
  key     = "${var.config_key_prefix}user_data.sh"
  content = "${data.template_file.user_data.rendered}"
}

resource "aws_launch_configuration" "cluster" {
  lifecycle {
    create_before_destroy = true
  }

  depends_on = [
    "aws_s3_bucket_object.ecs_userdata",
    "aws_s3_bucket_object.ecs_config",
  ]

  name_prefix          = "${local.qualified_name}-"
  image_id             = "${var.ami_id_for_region[var.region]}"
  instance_type        = "${var.instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.instance_profile.name}"

  security_groups = [
    "${concat(var.instance_security_group_ids, list(aws_security_group.cluster.id))}",
  ]

  key_name  = "${var.instance_key_name}"
  user_data = "${data.template_file.user_data_bootstrap.rendered}"
}
