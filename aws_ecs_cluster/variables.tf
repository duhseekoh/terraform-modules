variable "env" {}
variable "region" {}
variable "name" {}

variable "vpc_id" {}
variable "config_bucket" {}
variable "config_key_prefix" {}

variable "cluster_max_size" {
  default = 1
}

variable "cluster_min_size" {
  default = 1
}

variable "cluster_desired_size" {
  default = 1
}

variable "instance_type" {
  default = "t2.micro"
}

variable "instance_key_name" {}

variable "instance_security_group_ids" {
  type = "list"
}

variable "cluster_subnet_ids" {
  type = "list"
}

variable "ami_id_for_region" {
  default = {
    ap-northeast-1 = "ami-a99d8ad5"
    ap-northeast-2 = "ami-9d56f9f3"
    ap-south-1     = "ami-72edc81d"
    ap-southeast-1 = "ami-846144f8"
    ap-southeast-2 = "ami-efda148d"
    ca-central-1   = "ami-897ff9ed"
    eu-central-1   = "ami-9fc39c74"
    eu-west-1      = "ami-2d386654"
    eu-west-2      = "ami-2218f945"
    eu-west-3      = "ami-250eb858"
    sa-east-1      = "ami-4a7e2826"
    us-east-1      = "ami-aff65ad2"
    us-east-2      = "ami-64300001"
    us-west-1      = "ami-69677709"
    us-west-2      = "ami-40ddb938"
  }
}
