output "cluster_id" {
  value = "${aws_ecs_cluster.cluster.id}"
}

output "security_group_id" {
  value = "${aws_security_group.cluster.id}"
}

output "subnet_ids" {
  value = ["${aws_autoscaling_group.cluster.vpc_zone_identifier}"]
}

output "cluster_role_arn" {
  value = "${aws_iam_role.cluster_role.arn}"
}

output "cluster_role_name" {
  value = "${aws_iam_role.cluster_role.name}"
}
