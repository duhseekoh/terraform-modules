data "template_file" "user_data" {
  template = "${file("${path.module}/user_data.tpl.sh")}"

  vars {
    s3_config_path = "${var.config_bucket}/${var.config_key_prefix}"
  }
}

data "template_file" "user_data_bootstrap" {
  template = "${file("${path.module}/user_data_bootstrap.tpl.sh")}"

  vars {
    s3_config_path = "${var.config_bucket}/${var.config_key_prefix}"
  }
}
