#!/bin/bash
set -e

# minimal bootstrap to initiate user data
yum -y update
yum install -y aws-cli

# minimal bootstrap to initiate user data
aws s3 cp s3://${s3_config_path}user_data.sh /usr/local/src/user_data.sh
chmod u+x /usr/local/src/user_data.sh
/usr/local/src/user_data.sh