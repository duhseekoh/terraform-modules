resource "aws_vpc" "vpc" {
  cidr_block           = "${var.cidr_block}"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name      = "${var.vpc_name}"
    terraform = "true"
  }
}

// Security Groups

resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name      = "${var.vpc_name}-default"
    terraform = "true"
  }
}

resource "aws_security_group" "default_private" {
  name   = "default-private"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name      = "${var.vpc_name}-default-private"
    terraform = "true"
  }
}

resource "aws_security_group" "default_public" {
  name   = "default-public"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name      = "${var.vpc_name}-default-public"
    terraform = "true"
  }
}

resource "aws_security_group_rule" "default_egress_private" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.default_private.id}"
}

resource "aws_security_group_rule" "default_egress_public" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.default_public.id}"
}

// Subnets

resource "aws_subnet" "private" {
  count                   = "${length(var.private_subnets)}"
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.private_subnets[count.index]}"
  availability_zone       = "${var.subnet_availability_zones[count.index]}"
  map_public_ip_on_launch = false

  tags = {
    Name      = "${var.vpc_name}-${var.subnet_availability_zones[count.index]}-private"
    terraform = "true"
  }
}

resource "aws_subnet" "public" {
  count                   = "${length(var.public_subnets)}"
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.public_subnets[count.index]}"
  availability_zone       = "${var.subnet_availability_zones[count.index]}"
  map_public_ip_on_launch = true

  tags = {
    Name      = "${var.vpc_name}-${var.subnet_availability_zones[count.index]}-public"
    terraform = "true"
  }
}

// Internet Gateway

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name      = "${var.vpc_name}"
    terraform = "true"
  }
}

// NAT Gateways

resource "aws_eip" "nat" {
  count = "${length(var.private_subnets)}"
  vpc   = true
}

resource "aws_nat_gateway" "nat_gateway" {
  count         = "${length(var.public_subnets)}"
  allocation_id = "${element(aws_eip.nat.*.id, count.index)}"
  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"
}

// Routing

resource "aws_route_table" "public" {
  count  = "${length(var.public_subnets)}"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name      = "${var.vpc_name}-${element(var.subnet_availability_zones, count.index)}-public"
    terraform = "true"
  }
}

resource "aws_route_table" "private" {
  count  = "${length(var.private_subnets)}"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name      = "${var.vpc_name}-${element(var.subnet_availability_zones, count.index)}-private"
    terraform = "true"
  }
}

resource "aws_route" "public_internet_gateway" {
  count                  = "${length(var.public_subnets)}"
  route_table_id         = "${element(aws_route_table.public.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.internet_gateway.id}"
  depends_on             = ["aws_route_table.public"]
}

resource "aws_route" "private_nat" {
  count                  = "${length(var.private_subnets)}"
  route_table_id         = "${element(aws_route_table.private.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.nat_gateway.*.id, count.index)}"
  depends_on             = ["aws_route_table.private"]
}

resource "aws_route_table_association" "public" {
  count          = "${length(var.public_subnets)}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.public.*.id, count.index)}"
}

resource "aws_route_table_association" "private" {
  count          = "${length(var.private_subnets)}"
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
}

resource "aws_route53_zone" "private_zone" {
  name   = "${var.vpc_name}"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    terraform = "true"
  }
}
