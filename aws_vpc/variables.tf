variable vpc_name {}

variable cidr_block {}

variable subnet_availability_zones {
  type = "list"
}

variable private_subnets {
  type = "list"
}

variable public_subnets {
  type = "list"
}
