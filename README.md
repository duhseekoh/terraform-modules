terraform-modules
=================

A collection of [Terraform](https://terraform.io) modules to standardize our infrastructure.

## Conventions

### Module Naming

Working on the assumption that each module probably deals with a single provider, it makes sense to prefix the modules by provider. So all AWS modules begin with `aws_`, any kubernetes ones might start with `k8s_`, etc.

### Module Structure

Each module should include the following:

- `README.md` - A README explaining the module's purpose, usage, and limitaitons.
- `main.tf` - The main Terraform file containing the resources managed by this module.
- `variables.tf` - A Terraform file containing the variables accepted by this module.
- `outputs.tf` - A Terraform file containing the attributes exposed by this module.
