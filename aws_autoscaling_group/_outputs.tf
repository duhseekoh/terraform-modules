output "instance_iam_role_id" {
  value = "${aws_iam_role.role.id}"
}

output "security_group_id" {
  value = "${aws_security_group.group.id}"
}
