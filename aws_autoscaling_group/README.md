aws_autoscaling_group
=====================

Creates and manages:

- A new autoscaling group
- A security group to which instances in this group belong
