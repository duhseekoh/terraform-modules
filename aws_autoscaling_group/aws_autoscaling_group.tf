locals {
  qualified_name = "${join("-", concat(var.prefixes, list(var.name)))}"
  path           = "/${join("/", concat(var.prefixes))}/"
}

data "template_file" "default_instance_user_data" {
  template = "${file("${path.module}/templates/default_instance_user_data.tpl")}"
}

data "aws_ami" "default" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "description"
    values = ["Amazon Linux*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_security_group" "group" {
  name   = "${local.qualified_name}"
  vpc_id = "${var.vpc_id}"

  tags = {
    "Name"      = "${local.qualified_name}"
    "terraform" = "true"
  }
}

resource "aws_iam_role" "role" {
  name = "${local.qualified_name}"
  path = "${local.path}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "profile" {
  name = "${local.qualified_name}"
  path = "${local.path}"
  role = "${aws_iam_role.role.name}"
}

resource "aws_launch_configuration" "config" {
  name_prefix          = "${local.qualified_name}-"
  image_id             = "${coalesce(var.ami_id, data.aws_ami.default.id)}"
  instance_type        = "${var.instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.profile.name}"
  key_name             = "${var.key_pair_name}"

  user_data = "${coalesce(var.user_data, data.template_file.default_instance_user_data.rendered)}"

  lifecycle {
    create_before_destroy = true
  }

  security_groups = [
    "${concat(var.security_group_ids, list(aws_security_group.group.id))}",
  ]
}

resource "aws_autoscaling_group" "asg" {
  vpc_zone_identifier  = ["${var.subnet_ids}"]
  name                 = "${local.qualified_name}"
  min_size             = "${var.size_min}"
  max_size             = "${var.size_max}"
  desired_capacity     = "${var.size_desired}"
  force_delete         = true
  launch_configuration = "${aws_launch_configuration.config.name}"

  tags = [
    {
      key                 = "terraform"
      value               = "true"
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "${local.qualified_name}"
      propagate_at_launch = true
    },
  ]

  tags = ["${var.tags}"]
}
