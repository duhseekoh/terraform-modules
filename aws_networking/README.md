aws_networking
==============

Configures essential AWS networking and infrastructure.

For variables, see `_variables.tf`.

For outputs, see `_outputs.tf`.

Provides:

- A VPC
- A private Route 53 zone
- _N_ public subnets (distributed across _N_ availability zones) with an Internet Gateway for egress
- _N_ private subnets (distributed across _N_ availability zones) with NAT Gateways for egress
- Default public and private security groups that enable egress to the 0.0.0.0
