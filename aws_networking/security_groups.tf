resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"      = "${var.vpc_name}-default"
    "terraform" = "true"
  }
}

resource "aws_security_group" "default_private" {
  name   = "${var.vpc_name}-default-private"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"      = "${var.vpc_name}-default-private"
    "terraform" = "true"
  }
}

resource "aws_security_group" "default_public" {
  name   = "${var.vpc_name}-default-public"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"      = "${var.vpc_name}-default-public"
    "terraform" = "true"
  }
}

resource "aws_security_group_rule" "default_egress_private" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.default_private.id}"
}

resource "aws_security_group_rule" "default_egress_public" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.default_public.id}"
}
