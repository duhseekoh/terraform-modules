resource "aws_subnet" "private" {
  count = "${length(var.private_subnets)}"

  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.private_subnets[count.index]}"
  availability_zone       = "${var.subnet_availability_zones[count.index]}"
  map_public_ip_on_launch = false

  tags = {
    "Name"      = "${var.vpc_name}-${var.subnet_availability_zones[count.index]}-public"
    "terraform" = "true"
  }
}

resource "aws_route_table" "private" {
  count = "${length(var.private_subnets)}"

  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    "Name"      = "${var.vpc_name}-${var.subnet_availability_zones[count.index]}-private"
    "terraform" = "true"
  }
}

resource "aws_route_table_association" "private" {
  count = "${length(var.private_subnets)}"

  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
}

resource "aws_eip" "nat" {
  count = "${length(var.private_subnets)}"
  vpc   = true
}

## Provides route to Internet for private subnets
resource "aws_nat_gateway" "nat" {
  # The NATs go _in_ the public subnet, but ultimately provide routing to the
  # _private_ subnet.
  count = "${length(var.public_subnets)}"

  allocation_id = "${element(aws_eip.nat.*.id, count.index)}"
  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"
}

resource "aws_route" "private_to_internet" {
  count = "${length(var.private_subnets)}"

  route_table_id         = "${element(aws_route_table.private.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.nat.*.id, count.index)}"
}
