variable "vpc_name" {
  type = "string"
}

variable "cidr_block" {
  default = "10.0.0.0/16"
}

variable "private_subnets" {
  type    = "list"
  default = ["10.0.0.0/20", "10.0.16.0/20"]
}

variable "public_subnets" {
  type    = "list"
  default = ["10.0.255.0/26", "10.0.255.64/26"]
}

variable "subnet_availability_zones" {
  type        = "list"
  description = "must specify the same number of zones as subnets"
}
