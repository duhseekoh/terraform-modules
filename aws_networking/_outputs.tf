output "security_group_id_default" {
  value = "${aws_default_security_group.default.id}"
}

output "security_group_id_default_private" {
  value = "${aws_security_group.default_private.id}"
}

output "security_group_id_default_public" {
  value = "${aws_security_group.default_public.id}"
}

output "subnet_ids_private" {
  value = ["${aws_subnet.private.*.id}"]
}

output "subnet_ids_public" {
  value = ["${aws_subnet.public.*.id}"]
}

output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "route53_zone_id" {
  value = "${aws_route53_zone.zone.zone_id}"
}
