locals {
  prefixed_stream_name = "${join("-", var.prefixes)}-${var.name}"
}

resource "aws_kinesis_stream" "stream" {
  name        = "${local.prefixed_stream_name}"
  shard_count = "${var.shard_count}"
}

resource "aws_iam_policy" "stream_producer" {
  name        = "${local.prefixed_stream_name}-producer"
  description = "Grants write-access to ${local.prefixed_stream_name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "kinesis:PutRecord",
        "kinesis:PutRecords"
      ],
      "Effect": "Allow",
      "Resource": "${aws_kinesis_stream.stream.arn}"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "stream_consumer" {
  name        = "${local.prefixed_stream_name}-consumer"
  description = "Grants read-access to ${local.prefixed_stream_name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "kinesis:DescribeStream",
        "kinesis:GetRecords",
        "kinesis:GetShardIterator",
        "kinesis:ListStreams"
      ],
      "Effect": "Allow",
      "Resource": "${aws_kinesis_stream.stream.arn}"
    }
  ]
}
EOF
}
