variable "prefixes" {
  type        = "list"
  description = "Prefixes for this stream. E.g., '[stackname, env]'. Will be joined with an appropriate separator."
}

variable "name" {
  type        = "string"
  description = "General name for this stream. Actual name will be '$prefix-$name'"
}

variable "shard_count" {
  type    = "string"
  default = 1
}
