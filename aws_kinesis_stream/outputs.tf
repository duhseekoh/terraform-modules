output "consumer_policy_arn" {
  value = "${aws_iam_policy.stream_consumer.arn}"
}

output "producer_policy_arn" {
  value = "${aws_iam_policy.stream_producer.arn}"
}

output "stream_name" {
  value = "${aws_kinesis_stream.stream.name}"
}

output "stream_arn" {
  value = "${aws_kinesis_stream.stream.arn}"
}

output "stream_shard_count" {
  value = "${aws_kinesis_stream.stream.shard_count}"
}
