variable "env" {}
variable "name" {}

variable "domain" {}

variable "vpc_id" {}

variable "security_group_ids" {
  type        = "list"
  description = "additional security groups to associate this ALB with"
}

variable "subnet_ids" {
  type        = "list"
  description = "subnets to run this ALB in"
}
