locals {
  qualified_name = "${var.env}-${var.name}"
}

resource "aws_security_group" "security_group" {
  vpc_id = "${var.vpc_id}"

  tags = {
    Name      = "${local.qualified_name}"
    terraform = "true"
  }
}

resource "aws_security_group_rule" "expose_http" {
  security_group_id = "${aws_security_group.security_group.id}"
  type              = "ingress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "expose_https" {
  security_group_id = "${aws_security_group.security_group.id}"
  type              = "ingress"
  from_port         = "443"
  to_port           = "443"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_alb" "alb" {
  name     = "${local.qualified_name}-alb"
  internal = false

  security_groups = [
    "${concat(var.security_group_ids, list(aws_security_group.security_group.id))}",
  ]

  subnets = ["${var.subnet_ids}"]
}

resource "aws_alb_target_group" "default_http" {
  name     = "${local.qualified_name}-http"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    matcher = "200-499"
  }
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.default_http.arn}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "https" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${data.aws_acm_certificate.certificate.arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.default_http.arn}"
    type             = "forward"
  }
}
