output "alb_arn" {
  value = "${aws_alb.alb.arn}"
}

output "security_group_id" {
  value = "${aws_security_group.security_group.id}"
}

output "default_target_group_arn_http" {
  value = "${aws_alb_target_group.default_http.arn}"
}

output "public_dns" {
  value = "${aws_alb.alb.dns_name}"
}
