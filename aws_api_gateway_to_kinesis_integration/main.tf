resource "aws_iam_policy" "write_to_stream" {
  name = "write-to-${var.stream_name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Effect": "Allow",
        "Action": "kinesis:*",
        "Resource": "${var.stream_arn}"
    }
  ] 
}
EOF
}

resource "aws_iam_role_policy_attachment" "kinesis_policy" {
  role       = "${var.integration_role_name}"
  policy_arn = "${aws_iam_policy.write_to_stream.arn}"
}

resource "aws_api_gateway_integration" "kinesis_integration" {
  rest_api_id             = "${var.api_gateway_id}"
  resource_id             = "${var.resource_id}"
  http_method             = "POST"
  type                    = "AWS"
  integration_http_method = "POST"
  uri                     = "arn:aws:apigateway:${var.region}:kinesis:action/PutRecord"
  passthrough_behavior    = "WHEN_NO_TEMPLATES"
  credentials             = "${var.integration_role_arn}"

  request_parameters = {
    "integration.request.header.Content-Type" = "'application/x-amz-json-1.1'"
  }

  request_templates = {
    "application/json" = <<EOF
#set($data = "{""requestId"":""$context.requestId"",""body"":$input.json('$')}")
{
  "StreamName": "${var.stream_name}",
  "Data": "$util.base64Encode($data)",
  "PartitionKey": $input.json('$.coreid')
}
EOF
  }
}

resource "aws_api_gateway_integration_response" "put_record" {
  depends_on  = ["aws_api_gateway_integration.kinesis_integration"]
  rest_api_id = "${var.api_gateway_id}"
  resource_id = "${var.resource_id}"
  http_method = "${aws_api_gateway_integration.kinesis_integration.http_method}"
  status_code = "200"

  response_templates = {
    "application/json" = ""
  }
}
