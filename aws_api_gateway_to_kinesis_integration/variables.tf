variable integration_role_arn {}
variable integration_role_name {}

variable "stream_name" {}
variable "stream_arn" {}

variable "api_gateway_id" {}
variable "resource_id" {}
variable "region" {}
