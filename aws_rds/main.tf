resource "aws_db_instance" "default" {
  identifier              = "${var.identifier}"
  allocated_storage       = "${var.storage}"
  backup_retention_period = "${var.backup_retention_period}"
  engine                  = "${var.engine}"
  engine_version          = "${lookup(var.engine_version, var.engine)}"
  instance_class          = "${var.instance_class}"
  name                    = "${var.db_name}"
  username                = "${var.username}"
  password                = "${var.password}"

  vpc_security_group_ids = [
    "${var.vpc_security_group_ids}",
  ]

  db_subnet_group_name = "${aws_db_subnet_group.default.id}"

  multi_az = "${var.multi_az}"

  tags = {
    "terraform" = "true"
  }
}

resource "aws_db_subnet_group" "default" {
  name        = "main_subnet_group"
  description = "Our main group of subnets"

  subnet_ids = [
    "${var.subnet_ids}",
  ]
}
