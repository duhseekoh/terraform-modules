variable "backup_retention_period" {
  description = "The days to retain backups for. Must be 1 or greater to be a source for a Read Replica."

  default = "7"
}

variable "db_name" {
  description = "db name"
  type        = "string"
}

variable "engine" {
  description = "Engine type, example values: mysql, postgres. For other engine types, see: https://amzn.to/2HKTLOG"

  default = "postgres"
}

variable "engine_version" {
  description = "Engine version"
  type        = "map"

  default = {
    mysql    = "5.7.21"
    postgres = "9.6.6"
  }
}

variable "identifier" {
  description = "Identifier for your DB"
  type        = "string"
}

variable "instance_class" {
  description = "Instance class"

  default = "db.t2.micro"
}

variable "multi_az" {
  description = "Specifies if the RDS instance is multi-AZ"

  default = "true"
}

variable "password" {
  description = "password, provide through your ENV variables"
  type        = "string"
}

variable "storage" {
  description = "Storage size in GB"

  default = "10"
}

variable "subnet_ids" {
  description = "Security Group IDs"
  type        = "list"
}

variable "username" {
  description = "User name"

  default = "aws_admin"
}

variable "vpc_security_group_ids" {
  description = "Security Group IDs"
  type        = "list"
}
