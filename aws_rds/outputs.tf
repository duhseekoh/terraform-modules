output "db_instance_address" {
  value = "${aws_db_instance.default.address}"
}

output "db_instance_id" {
  value = "${aws_db_instance.default.id}"
}

output "db_subnet_group_name" {
  value = "${aws_db_subnet_group.default.name}"
}

output "db_engine" {
  value = "${aws_db_instance.default.engine}"
}

output "db_address" {
  value = "${aws_db_instance.default.address}"
}

output "db_name" {
  value = "${aws_db_instance.default.name}"
}

output "db_port" {
  value = "${aws_db_instance.default.port}"
}
