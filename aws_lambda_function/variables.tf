variable "env" {}
variable "name" {}
variable "runtime" {}
variable "s3_bucket" {}
variable "handler" {}

variable "environment" {
  type = "map"
}

variable "timeout" {
  default = 3
}
