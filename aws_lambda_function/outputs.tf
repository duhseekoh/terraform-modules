output "function_arn" {
  value = "${aws_lambda_function.lambda_function.arn}"
}

output "function_name" {
  value = "${var.name}"
}

output "function_role_name" {
  value = "${aws_iam_role.role.name}"
}
