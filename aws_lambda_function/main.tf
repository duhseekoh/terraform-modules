resource "aws_lambda_function" "lambda_function" {
  function_name = "${var.env}-${var.name}"
  runtime       = "${var.runtime}"
  s3_bucket     = "${var.s3_bucket}"
  s3_key        = "deploy/${var.env}/${var.name}.zip"
  handler       = "${var.handler}"
  role          = "${aws_iam_role.role.arn}"
  timeout       = "${var.timeout}"

  environment {
    variables = "${var.environment}"
  }
}

resource "aws_iam_role" "role" {
  name = "${var.env}-${var.name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_basic_execution_role" {
  role       = "${aws_iam_role.role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
